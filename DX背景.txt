#include "DxLib.h"

#define PI 3.141592654
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	ChangeWindowMode(true);
	SetDrawScreen(DX_SCREEN_BACK);
	DxLib_Init(); // DXライブラリ初期化処理
	//LoadGraphScreen(50, 100, "画像/キャラクタ00.png", TRUE); // 画像を描画する


	int Handle1 = LoadGraph("画像/キャラクタ00.png");
	int Handle2 = LoadGraph("画像/キャラクタ01.png");

	//DrawGraph(295, 215, Handle1, true);
	int Blue = GetColor(0, 0, 255);
	int x = 0;
	int bx = 50, by = 50;
	int Pal = 50;
	int kx = 320, ky = 240;
	int Handle00, Handle01, Handle02; // 画像格納用ハンドル
	Handle00 = LoadGraph("画像/キャラクタ00.png"); // 画像のロード
	Handle01 = LoadGraph("画像/キャラクタ01.png"); // 画像のロード
	Handle02 = LoadGraph("画像/メニュー背景_マスク.png"); // 画像のロード
	int Handle04 = LoadGraph("画像/ジャグジャグ.png");
	int Handle03 = LoadGraph("画像/フクイデ先生.png");
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0) {
		int x = 100;
		//int Handle; // 画像格納用ハンドル
		// while(裏画面を表画面に反映, メッセージ処理, 画面クリア)
		while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0) {
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			DrawRotaGraph(kx, ky, 1.0,0,Handle02, true);
			DrawRotaGraph(kx - 640, ky, 1.0, 0, Handle03, true);
			DrawRotaGraph(kx - 1280, ky, 1.0, 0, Handle04, true);
			DrawRotaGraph(kx - 1920, ky, 1.0, 0, Handle02, true);
			if (kx >= 640 + 1600) {
				kx = 320;
			}
			kx++;
			
			//SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); //ブレンドモードをオフ
			//DrawRotaGraph(400, 200, 2.0, PI / 4, Handle1, TRUE); //画像の描画
			//SetDrawBlendMode(DX_BLENDMODE_ALPHA, 150); //ブレンドモードをα(128/255)に設定
			//DrawRotaGraph(x, 200, 1.0, 0.0, Handle1, TRUE); //画像の描画
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			for (int i = 0;i < 10;i++) {
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 50 + (i * 20)); //αブレンド
				DrawCircle(bx + (i * 40), by + 20, 20, Blue, TRUE);

				SetDrawBlendMode(DX_BLENDMODE_ADD, 50 + (i * 20)); //加算ブレンド
				DrawCircle(bx + (i * 40), by + 60, 20, Blue, TRUE);

				SetDrawBlendMode(DX_BLENDMODE_SUB, 50 + (i * 20)); //減算ブレンド
				DrawCircle(bx + (i * 40), by + 100, 20, Blue, TRUE);

				SetDrawBlendMode(DX_BLENDMODE_MULA, 50 + (i * 20)); //乗算ブレンド
				DrawCircle(bx + (i * 40), by + 140, 20, Blue, TRUE);

				SetDrawBlendMode(DX_BLENDMODE_INVSRC, 50 + (i * 20)); //反転ブレンド
				DrawCircle(bx + (i * 40), by + 180, 20, Blue, TRUE);

				//Pal += 10;
			}
			x++; // xを1増やす
			if (x == 640) {
				x = 0;
			}
		}
	}
}